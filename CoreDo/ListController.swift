
//  http://jamesonquave.com/blog/core-data-in-swift-tutorial-part-1/

import UIKit
import CoreData

class ListController: UITableViewController {
    
    var managedObjectContext:NSManagedObjectContext?
    
    var items = [Item]()

    @IBAction func addItem(sender: UIBarButtonItem) {
        println("addItem")
        var itemField:UITextField?
        var alert = UIAlertController(title: "New Item", message: "Type item below", preferredStyle: UIAlertControllerStyle.Alert)
        alert.addTextFieldWithConfigurationHandler({
            (textField) -> Void in
            itemField = textField
        })
        alert.addAction(UIAlertAction(title: "Add", style: UIAlertActionStyle.Default, handler: {
                (action) -> Void in
                    println("Add button pressed")
                    //println(itemField?.text)
                    if let item = itemField?.text {
                        println(item)
                        self.addItem(item, itemQuantity: 1, itemCompleted: false)
                        self.searchItems()
                        self.tableView.reloadData()
                    }
        }))
        
        self.presentViewController(alert, animated: true, completion: nil)
        
        
    }
    
    func addItem(itemName:String, itemQuantity:Int, itemCompleted:Bool) {
        let newItem:Item = NSEntityDescription.insertNewObjectForEntityForName("Item", inManagedObjectContext: self.managedObjectContext!) as Item
        newItem.name = itemName
        newItem.quantity = itemQuantity
        newItem.completed = itemCompleted
        
        let fetchRequest = NSFetchRequest(entityName: "Item")
        if let fetchResults = self.managedObjectContext!.executeFetchRequest(fetchRequest, error: nil) as? [CoreDo.Item] {
            
            let alert = UIAlertView()
            for item:Item in fetchResults {
                println("Item in store: \(item.name)")
            }
        }
    }
    
    func searchItems(term:String) {
        let predicate = NSPredicate(format: "start_date contains[search] \(term)")
        let fetchRequest = NSFetchRequest(entityName: "Item")
        let sortDescriptor:NSSortDescriptor = NSSortDescriptor(key: "number", ascending: true)
        let sortArray:NSArray = [sortDescriptor]
        fetchRequest.sortDescriptors = sortArray
        fetchRequest.predicate = predicate
        if let fetchResults = managedObjectContext!.executeFetchRequest(fetchRequest, error: nil) as? [Item] {
            self.items = fetchResults
        }
    }
    
    func searchItems() {
        let fetchRequest = NSFetchRequest(entityName: "Item")
        if let fetchResults = managedObjectContext!.executeFetchRequest(fetchRequest, error: nil) as? [Item] {
            self.items = fetchResults
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let appDelegate = UIApplication.sharedApplication().delegate as AppDelegate
        if let context = appDelegate.managedObjectContext {
            self.managedObjectContext = context
            println(context)
        }
        let homeDir:String = NSHomeDirectory()
        println(homeDir);
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.items.count
    }

    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("Item", forIndexPath: indexPath) as UITableViewCell
        let item = self.items[indexPath.row]
        cell.textLabel.text = item.name
        cell.detailTextLabel?.text = "\(item.quantity)"

        return cell
    }

    /*
    // Override to support conditional editing of the table view.
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return NO if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            // Delete the row from the data source
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
        } else if editingStyle == .Insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(tableView: UITableView, moveRowAtIndexPath fromIndexPath: NSIndexPath, toIndexPath: NSIndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return NO if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using [segue destinationViewController].
        // Pass the selected object to the new view controller.
    }
    */

}

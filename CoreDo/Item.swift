
import Foundation
import CoreData

class Item: NSManagedObject {

    @NSManaged var name: String
    @NSManaged var quantity: NSNumber
    @NSManaged var completed: NSNumber

}
